const path = require("path");

module.exports = {
  devtool: "source-map",
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      }
    ]
  },
  resolve: {
    root: [
      path.join(__dirname, "..")
    ],
    modulesDirectories: [
      "node_modules",
      "src/js"
    ]
  }
};
