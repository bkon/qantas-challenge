const webpack = require("webpack");

const UGLIFY_OPTIONS = {
  compress: {
    warnings: false
  }
};

function plugins() {
  if (process.env.NODE_ENV !== "production") {
    return [];
  }

  return [
    new webpack.DefinePlugin({
      "process.env": {
        "NODE_ENV": "\"production\""
      }
    }),
    new webpack.optimize.UglifyJsPlugin(UGLIFY_OPTIONS),
    new webpack.optimize.OccurenceOrderPlugin()
  ];
}

module.exports = Object.assign(
  {},
  require("./webpack.common"),
  {
    plugins: plugins(),
    externals: {
      // @see https://www.npmjs.com/package/slug
      // --ignore unicode/category/So
      "unicode/category/So": "{}"
    },
    output: {
      filename: "main.js"
    }
  }
);
