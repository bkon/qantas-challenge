const webpack = require("webpack");
const fs = require("fs");

// @see http://jlongster.com/Backend-Apps-with-Webpack--Part-I
// we don't want webpack packing code from node_modules
function nodeExternals() {
  var nodeModules = {};

  fs.readdirSync("node_modules")
    .filter(function filterBinaryModules(x) {
      return [".bin"].indexOf(x) === -1;
    })
    .forEach(function addExternal(mod) {
      nodeModules[mod] = "commonjs " + mod;
    });

  return nodeModules;
}

function plugins() {
  const bannerPlugin = new webpack.BannerPlugin(
    "require(\"source-map-support\").install();",
    { raw: true, entryOnly: false }
  );

  if (process.env.NODE_ENV !== "production") {
    return [bannerPlugin];
  }

  return [
    bannerPlugin,
    new webpack.DefinePlugin({
      "process.env": {
        "NODE_ENV": "\"production\""
      }
    })
  ]
}

module.exports = Object.assign(
  {},
  require("./webpack.common"),
  {
    target: "node",
    plugins: plugins(),
    externals: nodeExternals(),
    output: {
      filename: "server.js"
    }
  }
);
