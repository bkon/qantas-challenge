/* global process */

const webpack = require("webpack");

module.exports = function configKarma(config) {
  const testConfig = {
    browsers: ["Chrome"],
    frameworks: [
      "mocha",
      "sinon",
      "chai",
      "sinon-chai"
    ],
    reporters: ["dots", "coverage"],

    files: [
      "tests/__index__.js"
//      "tests/__root__.js"
    ],

    exclude: [
      "**/flycheck_*"
    ],

    preprocessors: {
      "./tests/*.js": ["webpack"],
      "./src/js/**/*.js": ["webpack", "sourcemap"]
    },

    webpack: {
      watch: true,
      module: {
        preLoaders: [
          {
            test: /\.js$/,
            exclude: /tests|node_modules/,
            loader: "babel-loader"
          },
          {
            test: /\.js$/,
            include: /tests/,
            loader: "babel-loader"
          }
        ],
        loaders: [
          {
            test: /\.json$/,
            loader: "json"
          }
        ],
        noParse: [
          /node_modules\/sinon\//
        ]
      },
      resolve: {
        extensions: ["", ".js", ".jsx", ".json"],
        root: [
          __dirname
        ],
        modulesDirectories: [
          "src/js",
          "tests",
          "node_modules"
        ],
        alias: {
          "sinon": "sinon/pkg/sinon"
        }
      },
      plugins: [
        new webpack.IgnorePlugin(/ReactContext/)
      ],
      externals: {
        jsdom: "window",
        "react/lib/ExecutionEnvironment": true
      }
    },

    webpackMiddleware: {
      noInfo: true,
      stats: {
        colors: true
      }
    },

    coverageReporter: {
      instrumenters: {
        isparta: require("isparta")
      },
      instrumenter: {
        "src/js/**/*.js": "isparta"
      },
      dir: "tmp/coverage/",
      reporters: [
        { type: "html", subdir: "html" },
        { type: "lcovonly", subdir: "." }
      ]
    }
  };

  config.set(testConfig);
};
