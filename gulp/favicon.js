const gulp = require("gulp");

module.exports = function faviconTask() {
  return gulp.src("src/favicon.ico")
    .pipe(gulp.dest("www"));
}
