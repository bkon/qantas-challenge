/**
 * Wrap gulp streams into fail-safe function for better error reporting
 * Usage:
 * gulp.task('less', wrapPipe(function(success, error) {
 *   return gulp.src('less/*.less')
 *      .pipe(less().on('error', error))
 *      .pipe(gulp.dest('app/css'));
 * }));
 */

const notifier = require("node-notifier");

module.exports = function wrapPipe(taskFn) {
  return function wrapPipeCallback(done) {
    function onSuccess() {
      done();
    }

    function onError(err) {
      notifier.notify({
        title: "Error",
        message: err.stack
      });
      done(err);
    }

    const outStream = taskFn(onSuccess, onError);
    if(outStream && typeof outStream.on === 'function') {
      outStream.on('end', onSuccess);
    }
  }
}
