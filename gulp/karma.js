const gulp = require("gulp");
const Server = require("karma").Server;

module.exports = function karmaTask(done) {
  new Server({
    configFile: __dirname + '/../karma.conf.js',
    singleRun: true
  }, done).start();
}
