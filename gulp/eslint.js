const gulp = require("gulp");
const eslint = require("gulp-eslint");

module.exports = function eslintTask() {
  return gulp.src("src/js/**/*.js")
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}
