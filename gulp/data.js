const gulp = require("gulp");

module.exports = function dataTask() {
  return gulp.src("src/data/*.json")
    .pipe(gulp.dest("www/data"));
}
