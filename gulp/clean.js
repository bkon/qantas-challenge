const del = require("del");

module.exports = function cleanTask() {
  return Promise.all([
    del("www/**/rev-manifest.json"),
    del("www/css/*.css"),
    del("www/js/*.js"),
    del("www/**/*.map"),
    del("www/data/*.json"),
    del("www/images/*.png"),
    del("www/images/*.jpg"),
    del("www/images/*.svg"),
    del("www/*.html"),
    del("www/favicon.ico"),
    del("server/*.js"),
    del("server/*.map")
  ]);
}
