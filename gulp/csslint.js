const gulp = require("gulp");
const csslint = require("gulp-scss-lint");

module.exports = function csslintTask() {
  return gulp.src("src/css/**/*.css")
    .pipe(csslint({ bundleExec: true }));
}
