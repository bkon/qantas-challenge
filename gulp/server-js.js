const wrapGulpPipe = require("./utils/wrap-pipe");
const gulp = require("gulp");
const webpackStream = require("webpack-stream");
const webpackConfig = require("../config/webpack.server");

module.exports = wrapGulpPipe(function jsTask(success, error) {
  return gulp.src("src/js/server.js")
    .pipe(webpackStream(webpackConfig).on("error", error))
    .pipe(gulp.dest("server"));
});
