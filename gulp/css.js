const wrapGulpPipe = require("./utils/wrap-pipe");
const gulp = require("gulp");
const sass = require("gulp-sass");
const RevAll = require("gulp-rev-all");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const moduleImporter = require("sass-module-importer");
const fontMagician = require("postcss-font-magician");

const POSTCSS_PROCESSORS = [
  autoprefixer({ browsers: ["last 2 versions"] }),
  fontMagician,
  cssnano
];

function revAllOptions() {
  if (process.env.NODE_ENV !== "production") {
    return {
      dontRenameFile: [/main/]
    };
  }

  return {};
}

module.exports = wrapGulpPipe(function cssTask(success, error) {
  const revAll = new RevAll(revAllOptions());

  return gulp.src("src/css/main.css")
    .pipe(sass({
      importer: [moduleImporter()],
      includePaths: ["."]
    }).on("error", error))
    .pipe(
      postcss(POSTCSS_PROCESSORS)
        .on("error", error)
    )
    .pipe(revAll.revision())
    .pipe(gulp.dest("www/css"))
    .pipe(revAll.manifestFile())
    .pipe(gulp.dest("www/css"));
});
