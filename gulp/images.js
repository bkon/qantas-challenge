const gulp = require("gulp");
const RevAll = require("gulp-rev-all");

const paths = [
  "src/images/*.png",
  "src/images/*.jpg",
  "src/images/*.svg"
];

module.exports = function imagesTask() {
  const revAll = new RevAll();

  return gulp.src(paths)
    .pipe(revAll.revision())
    .pipe(gulp.dest("www/images"))
    .pipe(revAll.manifestFile())
    .pipe(gulp.dest("www/images"));
}
