const wrapGulpPipe = require("./utils/wrap-pipe");
const gulp = require("gulp");
const RevAll = require("gulp-rev-all");
const webpackStream = require("webpack-stream");
const webpackConfig = require("../config/webpack.client");

function revAllOptions() {
  if (process.env.NODE_ENV !== "production") {
    return {
      dontRenameFile: [/main/]
    };
  }

  return {};
}

module.exports = wrapGulpPipe(function jsTask(success, error) {
  const revAll = new RevAll(revAllOptions());

  return gulp.src("src/js/main.js")
    .pipe(webpackStream(webpackConfig).on("error", error))
    .pipe(revAll.revision())
    .pipe(gulp.dest("www/js"))
    .pipe(revAll.manifestFile())
    .pipe(gulp.dest("www/js"));
});
