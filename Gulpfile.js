const gulp = require("gulp");
const server = require("gulp-live-server");

gulp.task("images", require("./gulp/images"));
gulp.task("favicon", require("./gulp/favicon"));
gulp.task("data", require("./gulp/data"));
gulp.task("client-js", require("./gulp/client-js"));
gulp.task("css", ["images"], require("./gulp/css"));
gulp.task("client", ["css", "client-js", "images", "favicon", "data"]);
gulp.task("server-js", require("./gulp/server-js"));
gulp.task("clean", require("./gulp/clean"));

gulp.task("watch", ["client"], function watchTask() {
  gulp.watch("src/css/**/*.css", ["css"]);
  gulp.watch("src/js/**/*.js", ["client-js"]);
  gulp.watch(
    ["src/images/**/*.png", "src/images/**/*.jpg", "src/images/*.svg"],
    ["images"]
  );
  gulp.watch("src/favicon.ico", ["favicon"]);
  gulp.watch("src/data/**/*.json", ["data"]);
})

gulp.task("server", ["server-js"], function serverTask() {
  const s = server.new("server/server.js");
  s.start();

  gulp.watch(["src/js/**/*.js"], ["server-js"]);

  gulp.watch(["www/**/*.css"], function watchAssets(file) {
      s.notify(file);
  });

  gulp.watch(["server/server.js"], function watchServer() {
    s.start();
  });
});

gulp.task("default", ["client", "server", "watch"]);

gulp.task("eslint", require("./gulp/eslint"));
gulp.task("csslint", require("./gulp/csslint"));
gulp.task("lint", ["eslint", "csslint"]);
gulp.task("karma", require("./gulp/karma"));
gulp.task("test", ["lint", "karma"]);
