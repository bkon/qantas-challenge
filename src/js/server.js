import express from "express";
import path from "path";
import favicon from "serve-favicon";
import logger from "morgan";
import liveReload from "connect-livereload";
import apiRoutes from "server/routes/api";
import appRoutes from "server/routes/app"

const cwd = process.cwd();
const app = express();
app.set("view engine", "pug");
app.set("views", path.join(cwd, "/src/pug"));

app.use(liveReload());
app.use(favicon(path.join(cwd, "www/favicon.ico")));
app.use(express.static(path.join(cwd, "www")));
app.use(logger("dev"));
app.use("/api", apiRoutes);
app.use("/", appRoutes);

app.use(function notFound(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

if (app.get("env") === "development") {
  app.use(require("./server/error-dev"));
} else {
  app.use(require("./server/error-prod"));
}

app.listen(3000);
