const logger = require("winston");

module.exports = function errorDev(err, req, res) {
  logger.error(err.stack);

  res.status(err.status || 500);
  res.json('error in development', {
    message: err.stack,
    error: err
  });
}
