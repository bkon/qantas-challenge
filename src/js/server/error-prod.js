module.exports = function errorProd(err, req, res) {
  res.status(err.status || 500);
  res.json('error in production', {
    message: err.message,
    error: {}
  });
}
