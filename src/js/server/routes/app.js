import express from "express";
import React from "react";
import { Provider } from "react-redux";
import createStore from "common/utils/create-store";
import { renderToString } from "react-dom/server";
import fs from "fs";
import loadGenres from "server/api/load-genres";
import loadMovies from "server/api/load-movies";
import routes from "components/routes";
import { RouterContext, match } from "react-router";

const router = express.Router()

function generateHelper(prefix, manifestPath) {
  const manifest = JSON.parse(fs.readFileSync(manifestPath));
  return function helper(key) {
    return prefix + "/" + manifest[key];
  }
}

function renderApp(res, renderProps, initialState) {
  const store = createStore(initialState);

  const html = renderToString((
    <Provider store={ store }>
      <RouterContext { ...renderProps }/>
    </Provider>
  ));

  const state = JSON.stringify(initialState);
  res.render("layout", {
    html,
    state,
    js: generateHelper("js", "www/js/rev-manifest.json"),
    css: generateHelper("css", "www/css/rev-manifest.json"),
    image: generateHelper("images", "www/images/rev-manifest.json")
  });
}

function matchRoute(url) {
  return new Promise(function promiseResolver(resolve, reject) {
    match(
      { routes: routes(), location: url },
      (error, redirectLocation, renderProps) => {
        if (error) {
          return reject(error);
        }

        if (redirectLocation) {
          return resolve({
            redirect: redirectLocation.pathname + redirectLocation.search
          });
        }

        if (!renderProps) {
          return reject("Not found");
        }

        return resolve({ renderProps });
      }
    );
  });
}

function renderIsomorphicView(res, renderProps, genres, genre) {
  loadMovies(genre)
    .then((movies) => {
      renderApp(
        res,
        renderProps,
        {
          filter: {
            genre: genre,
            genres: genres
          },
          movies: {
            items: movies
          }
        }
      );
    });
}

router.get(new RegExp('/([^/]+)?'), function appRoute(req, res, next) {
  Promise
    .all([
      matchRoute(req.url),
      loadGenres()
    ])
    .then(([{ renderProps, redirect }, genres]) => {
      if (redirect) {
        res.redirect(302, redirect);
        return null;
      }

      const genre = req.params[0] || genres[0];
      return renderIsomorphicView(
        res,
        renderProps,
        genres,
        genre
      );
    })
    .catch(next);
})

export default router;
