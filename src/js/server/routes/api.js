import express from "express";
import loadMovies from "server/api/load-movies";

const router = express.Router()

router.get(new RegExp('/movies/([^/]+)'), function moviesRoute(req, res) {
  const genre = req.params[0]

  loadMovies(genre)
    .then((data) => res.json(data));
});

export default router;
