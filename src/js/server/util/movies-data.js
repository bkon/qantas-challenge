import fs from "fs";

export default function allMovies() {
  return JSON.parse(fs.readFileSync("www/data/movies.json"));
}
