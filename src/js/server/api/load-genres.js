import R from "ramda";
import allMovies from "server/util/movies-data";

const compareStrings = R.comparator((a, b) => a < b);

const extractGenres = R.pipe(
  R.map(R.prop("genres")),
  R.flatten,
  R.uniq,
  R.sort(compareStrings)
)

export default function loadGenres() {
  return Promise.resolve(
    extractGenres(allMovies())
  );
}
