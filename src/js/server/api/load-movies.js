import R from "ramda";
import allMovies from "server/util/movies-data";
import normalizeGenre from "common/utils/normalize-genre";

const hasGenre = (genre) => R.pipe(
  R.prop("genres"),
  R.map(normalizeGenre),
  R.contains(normalizeGenre(genre))
);

export default function loadMovies(genre) {
  return Promise.resolve(
    R.pipe(
      R.filter(hasGenre(genre)),
      R.uniqBy(R.prop("title")) // JSON contains duplicate movies
    )(allMovies())
  );
}
