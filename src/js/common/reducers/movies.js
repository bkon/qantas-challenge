import createReducer from "common/utils/create-reducer";
import Types from "common/constants/actions";

const DEFAULT_STATE = {
  items: [],
  loading: false
};

export default createReducer(
  DEFAULT_STATE,
  {
    [Types.FETCH_MOVIES_PENDING]: (state) =>
      ({
        ...state,
        loading: true
      }),
    [Types.FETCH_MOVIES_DONE]: (state, action) =>
      ({
        ...state,
        loading: false,
        ...action.payload
      })
  }
);
