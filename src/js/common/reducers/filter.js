import createReducer from "common/utils/create-reducer";
import Types from "common/constants/actions";

const DEFAULT_STATE = {
  genre: [],
  genres: []
};

export default createReducer(
  DEFAULT_STATE,
  {
    [Types.CHANGE_GENRE_INTENT]: (state, action) =>
      ({
        ...state,
        ...action.payload
      })
  }
);
