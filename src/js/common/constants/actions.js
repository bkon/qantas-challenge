import keyMirror from "keymirror-symbol";

export default keyMirror({
  APP_BOOT_INTENT: null,
  CHANGE_GENRE_INTENT: null,

  FETCH_MOVIES_PENDING: null,
  FETCH_MOVIES_DONE: null
})
