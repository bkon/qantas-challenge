import normalizeGenre from "common/utils/normalize-genre";

function genreUrl(genre) {
  const genreSlug = normalizeGenre(genre);
  return `/${genreSlug}`;
}

export {
  genreUrl
}
