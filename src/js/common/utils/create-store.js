import { createStore, applyMiddleware, compose } from "redux";
import { reduxObservable } from "redux-observable";
import { routerMiddleware } from "react-router-redux";
import reducers from "common/reducers"
import { browserHistory } from "react-router";

export default function customCreateStore(initialState = {}) {
  return createStore(
    reducers,
    initialState,
    compose(
      applyMiddleware(reduxObservable()),
      applyMiddleware(routerMiddleware(browserHistory))
    )
  );
}
