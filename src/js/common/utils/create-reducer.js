import R from "ramda";

export default function createReducer(initialState, actionHandlers) {
  return (state = initialState, action) => {
    const reduceFn = actionHandlers[action.type] || R.identity;
    return reduceFn(state, action);
  };
}
