import R from "ramda";
import slug from "slug";

const normalizeGenre = R.pipe(
  slug,
  R.toLower
);

export default normalizeGenre;
