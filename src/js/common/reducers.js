import { combineReducers } from "redux";
import movies from "./reducers/movies";
import filter from "./reducers/filter";

export default combineReducers({
  movies,
  filter
});
