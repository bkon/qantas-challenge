import { PropTypes } from "react";
import genreType from "common/types/genre";

export default PropTypes.shape({
  genre: genreType.isRequired,
  genres: PropTypes.arrayOf(genreType).isRequired
})
