import { PropTypes } from "react";
import movieType from "common/types/genre";

export default PropTypes.shape({
  items: PropTypes.arrayOf(movieType).isRequired,
  loading: PropTypes.bool
})
