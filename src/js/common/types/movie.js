import { PropTypes } from "react";
import genreType from "common/types/genre";

export default PropTypes.shape({
  title: PropTypes.string.isRequired,
  genres: PropTypes.arrayOf(genreType).isRequired,
  rated: PropTypes.string,
  rating: PropTypes.number,
  language: PropTypes.arrayOf(PropTypes.string)
});
