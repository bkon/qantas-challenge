import { Observable as O } from "rxjs";
import fetchMoviesDone from "actions/fetch-movies-done";
import fetchMoviesPending from "actions/fetch-movies-pending";
import Actions from "common/constants/actions";
import loadMovies from "client/api/load-movies";

function rxMovieActions(actions) {
  return actions
    .ofType(Actions.CHANGE_GENRE_INTENT)
    .switchMap(
      ({ payload: { genre }}) => O
        .fromPromise(loadMovies(genre))
        .map(items => fetchMoviesDone(items))
        .startWith(fetchMoviesPending())
    );
}

export default function boot() {
  return (actions) =>
    O.merge(
      rxMovieActions(actions)
    )
}
