import Actions from "common/constants/actions";

export default function fetchMoviesDone(items) {
  return {
    type: Actions.FETCH_MOVIES_DONE,
    payload: {
      items
    }
  };
}
