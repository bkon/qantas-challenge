import Actions from "common/constants/actions";

export default function changeGenre(value) {
  return {
    type: Actions.CHANGE_GENRE_INTENT,
    payload: { genre: value }
  };
}
