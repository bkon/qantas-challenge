import { genreUrl } from "common/utils/route-helpers";
import { push } from "react-router-redux";

export default function changeGenreRoute(value) {
  return push(genreUrl(value));
}
