import Actions from "common/constants/actions";

export default function fetchMoviesPending() {
  return {
    type: Actions.FETCH_MOVIES_PENDING
  };
}
