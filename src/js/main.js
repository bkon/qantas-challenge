import React from "react";
import { render } from "react-dom";
import createStore from "common/utils/create-store";
import { Provider } from "react-redux";
import appBoot from "actions/app-boot";
import { Router, browserHistory } from "react-router";
import routes from "components/routes";
import window from "seams/window";
import document from "seams/document";

const store = createStore(window.$REDUX_STATE);

render(
  <Provider store={ store }>
    <Router history={ browserHistory }>
      { routes() }
    </Router>
  </Provider>,
  document.querySelector("#content")
);

store.dispatch(appBoot());
