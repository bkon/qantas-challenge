import fetch from "seams/fetch";
import normalizeGenre from "common/utils/normalize-genre";

export default function loadMovies(genre) {
  const genreSlug = normalizeGenre(genre);
  return fetch(`/api/movies/${genreSlug}`)
    .then(x => x.json());
}
