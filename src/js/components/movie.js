import R from "ramda";
import React, { PropTypes } from "react";
import { Link } from "react-router";
import { genreUrl } from "common/utils/route-helpers";
import genreType from "common/types/genre";

const Movie = ({ title, genres }) => (
  <div className="movie__container">
    <div className="movie__title">
      { title }
    </div>
    <div className="movie__genres">
      {
        R.map(
          genre => (
            <Link to={ genreUrl(genre) }
                  className="movie__genre"
                  key={ genre }>
              { genre }
            </Link>
          ),
          genres
        )
      }
    </div>
  </div>
);

Movie.propTypes = {
  genres: PropTypes.arrayOf(genreType).isRequired,
  title: PropTypes.string.isRequired
}

export default Movie;
