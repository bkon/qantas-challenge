import R from "ramda";
import React, { PropTypes } from "react";
import Movie from "components/movie";
import MovieType from "common/types/movie";

const MovieList = ({ items }) => (
  <div className="movie-list__container">
    {
      R.map(
        (movie) =>
          <Movie { ...movie }
                 key={ movie.title } />,
        items)
    }
  </div>
);

MovieList.propTypes = {
  items: PropTypes.arrayOf(MovieType).isRequired
}

export default MovieList;
