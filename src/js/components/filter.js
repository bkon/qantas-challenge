import R from "ramda";
import React, { PropTypes } from "react";
import { Link } from "react-router";
import { genreUrl } from "common/utils/route-helpers";
import normalizeGenre from "common/utils/normalize-genre";
import genreType from "common/types/genre";

function handleChange(handler) {
  return function onChange(e) {
    handler(e.target.value);
  }
}

const Filter = ({ genre, genres, onChangeGenre }) => (
  <div className="filter__container">
    <div className="filter__links">
      <Link className="filter__link"
            activeClassName="filter__link--selected"
            to={ genreUrl("action") }>
        Action
      </Link>
      <Link className="filter__link"
            activeClassName="filter__link--selected"
            to={ genreUrl("biography") }>
        Biography
      </Link>
    </div>

    <div className="filter__select">
      <select value={ normalizeGenre(genre) }
              onChange={ handleChange(onChangeGenre) }>
      {
        R.map(
          (genre) => (
            <option value={ normalizeGenre(genre) }
                    key={ genre }>
              { genre }
            </option>
          ),
          genres
        )
      }
      </select>
    </div>
  </div>
);

Filter.propTypes = {
  genre: genreType.isRequired,
  genres: PropTypes.arrayOf(genreType).isRequired,
  onChangeGenre: PropTypes.func.isRequired
}

export default Filter;
