import React, { PropTypes } from "react";
import Filter from "components/filter";
import MovieList from "components/movie-list";
import { connect } from "react-redux";
import pure from "pure-render-decorator";
import changeGenre from "actions/change-genre";
import changeGenreRoute from "actions/change-genre-route";
import moviesType from "common/types/movies";
import filterType from "common/types/filter";

@pure
class Component extends React.Component {
  componentDidMount() {
    const { onChangeGenre, params } = this.props;
    onChangeGenre(params.genre);
  }

  componentWillReceiveProps(newProps) {
    const { onChangeGenre, params } = this.props;
    if (newProps.params.genre != params.genre) {
      onChangeGenre(newProps.params.genre);
    }
  }

  render() {
    const { filter, movies, onChangeGenreRoute } = this.props;

    return (
      <div className="app__container">
        <div className="app__filter">
          <Filter { ...filter }
                  onChangeGenre={ onChangeGenreRoute }/>
        </div>

        <div className="app__movies">
          <MovieList { ...movies } />
        </div>
      </div>
    );
  }
}

Component.displayName = "App";

Component.propTypes = {
  params: PropTypes.shape({
    genre: PropTypes.string.isRequired
  }),
  filter: filterType.isRequired,
  movies: moviesType.isRequired,
  onChangeGenre: PropTypes.func.isRequired,
  onChangeGenreRoute: PropTypes.func.isRequired
};

Component.defaultProps = {
};

Component.contextTypes = {
};

export default connect(
  ({ filter, movies }) => {
    return { filter, movies };
  },
  (dispatch) => ({
    onChangeGenre: (val) => dispatch(changeGenre(val)),
    onChangeGenreRoute: (val) => dispatch(changeGenreRoute(val))
  })
)(Component);

export { Component };
