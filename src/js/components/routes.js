import React from "react";
import { Route, IndexRedirect } from "react-router";
import App from "components/app";

const Routes = () => (
  <Route path="/">
    <IndexRedirect to="/action"/>
    <Route path=":genre" component={ App }/>
  </Route>
)

export default Routes;
