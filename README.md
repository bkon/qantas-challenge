=== Prerequisites:

- nvm (node version manager)
- rbenv (optional; lint task dependency)

=== Installation steps:

```
# optional (lint task)
rbenv install
gem install bundler
bundle install

# required
nvm install
npm install
$(npm bin)/gulp
```

Gulp builds assets and runs development server on localhost:3000
(Alas, no HMR)

=== Misc tasks

```
NODE_ENV=production $(npm bin)/gulp
```

Builds compressed / fingerprinted assets and serves them on localhost:3000

```
$(npm bin)/gulp lint
```

Runs JS/CSS linter against `src`.

```
$(npm bin)/gulp clean
```

Removes precompiled assets in `www`.
